<html>
<body>

<B>Checkout</B><br>
Below is a summary of the products you wish to purchase, along with totals:
<?php

#tax rate is constant
$tax = 0.08;
$total_price = 0;
$total_tax = 0;
$total_shipping = 0;
$grand_total = 0;
?><ul><?

$hproduct = "Candle Holder";
$hprice = 11.95;
$hshipping = 0; //free shipping
echo "<li>".$hproduct.": $".$hprice;

//tally totals
function total($price,$tax,$shipping){
$total_price = $price;
$total_tax = $tax * $price;
$total_shipping = $shipping * $price; 
$sub_total = ($total_price + $total_tax + $total_shipping);
return $sub_total;
}

$grand_total+=total($hprice,$tax,$hshipping);

$cproduct = "Coffee Table";
$cprice = 99.50;
$cshipping = 0.10; //shipping as a percentage of price
echo "<li>".$cproduct.": $".$cprice;

//tally totals
$grand_total+= total($cprice,$tax,$cshipping);

$fproduct = "Floor Lamp";
$fprice = 44.99;
$fshipping = 0.10; //shipping as a percentage of price
echo "<li>".$fproduct.": $".$fprice;

//tally totals
$grand_total+=total($fprice,$tax,$fshipping);

//assign grand total to value returned by function

?>
</ul>
<hr>
<br>
<B>Total (including tax and shipping): $<? echo number_format($grand_total, 2); ?></B>

</body>
</html>

