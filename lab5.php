<html>
<body>
<?php
$subtotal  = 0;
$f = $_GET['food'];
function select_food($dinner)
{
/*food prices*/
$chickenprice = 8;
$steakprice = 10;
$salmonprice = 9;
$porkprice = 11;

//get food price according to food choice
if ($dinner=="steak")
{
return $steakprice;
}
else if ($dinner=="chicken")
{
return $chickenprice;

}
else if ($dinner=="salmon")
{
return $salmonprice;

}
else if ($dinner=="barbeque pork")
{
return $porkprice;
}
}

//choose beverage according to dinner choice
function select_beverage($dinner)
{
if ($dinner == "steak")
{
return "water";
}
else if ($dinner=="chicken")
{
return "soda";
}
else if ($dinner =="salmon")
{
return "juice";
}
else if ($dinner =="barbeque pork")
{
return "tea";
}
}

//get drink prices of chosen drink
function getdrinkprice($drink)
{
/*drink prices*/
$water= 2;
$soda= 1;
$juice= 3;
$tea = 2.5;

if ($drink=="water")
{
return $water;
}
else if ($drink == "soda")
{
return $soda;
}
else if ($drink == "juice")
{
return $juice;
}
else if ($drink == "tea")
{
return $tea;
}
}

$drinkprice = getdrinkprice(select_beverage($f));

/*tax and tip*/
$tax = 0.08;
$tip = 0.15;

/* find subtotal and total according to prices and choice */
$subtotal = select_food($f)+$drinkprice;
$grandtotal = $subtotal*(1+$tax+$tip);

/*display prices and totals */
print $f." - ".select_food($f)."<br>";
print select_beverage($f)." - ".$drinkprice."<br>";
/* print subtotal, tax, and tip */
print "Subtotal: ".$subtotal."<br> tax: $".$subtotal*$tax."<br> tip: $".$subtotal*$tip."<br>";
/* print grand total */
print "Grand total: $".$grandtotal;

?>

</body>
</html>